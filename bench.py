import os
import crypto 
import validator as valid
import system as sys
from cryptography.hazmat.primitives.ciphers.algorithms import AES, Blowfish, SEED
from cryptography.hazmat.primitives.ciphers.modes import OFB

if __name__ == '__main__':
    sys.info()
    os.chdir('/home/christ0s/Nextcloud/EEDUTH/8ο_Εξάμηνο/Cryptography/Project')
    filename = 'urand_testfile'
    
    algs = [AES, Blowfish, SEED]
    mode = OFB
    key_lens = [32,32,16]
    iv_lens = [16,8,16]

    for i in range(len(algs)):
        print()
        crypto.cipher = crypto.key_gen(algs[i], key_lens[i], iv_lens[i], mode)
        crypto.enc(filename, algs[i].name)
        crypto.dec(algs[i].name)
        valid.sha_match(valid.sha(filename),valid.sha(algs[i].name+'_decrypted'))

