import os
import numpy as np
import matplotlib.pyplot as plt
algs = ['AES','Blowfish','SEED']

def mean_time(cpu):
    os.chdir('/home/christ0s/Nextcloud/EEDUTH/8ο_Εξάμηνο/Cryptography/Project/'+cpu)
    Mean = []
    for i in range(len(algs)):
        data = np.loadtxt(algs[i]+'.data')
        lines = int(data.size/data[0].size)
        S = []
        mean = []
        for i in range(data[0].size):
            s = 0.0
            for j in range(lines):
                s += data[j][i]
            S.append(s)    
        for i in S:
            mean.append(i/lines)
        Mean.append(mean)
    print(np.array(Mean))
    return(np.array(Mean))

def plot(alg,wi, T):
    os.chdir('/home/christ0s/Nextcloud/EEDUTH/8ο_Εξάμηνο/Cryptography/Project/')
    colors=['#737373','r','#ff8d1e','#737373','#005fbb','#ff8d1e']
    cpus = ['R73G', 'R74G', 'I52G','ARM']
    
    for i in range(len(T)):
        for j in range(len(T[i])):
            if j < 3:
                if j == 0:
                    plt.bar(i,T[i][j],width = wi,color=colors[j])
                else: 
                    plt.bar(i,T[i][j],width = wi, bottom=T[i][j-1],color=colors[j])
            else:
                if j == 3:
                    plt.bar(i+wi+0.05,T[i][j],width = wi,color=colors[j])
                else:
                    plt.bar(i+wi+0.05,T[i][j],width = wi, bottom=T[i][j-1],color=colors[j])
    
    Pos = np.array(range(len(T)))
    plt.xticks(Pos+wi/2, cpus)
    plt.grid(color='gray', linestyle='--', linewidth=0.3)
    plt.title(alg)
    plt.ylabel('Time(s)')
    plt.xlabel('CPU')
    plt.savefig(alg+'.png',dpi=1200,bbox_inches="tight")

if __name__ == '__main__':
    R73G = mean_time('R73G')
    R74G = mean_time('R74G')
    I52G = mean_time('I52G')
    ARM = mean_time('ARM')
    width = 0.35
   
    #for i in range(len(algs)):
    #    T = R73G[i],R74G[i],I52G[i],ARM[i]
    #    plot(algs[i],width,T)
    
    #N = R73G[0],R74G[0],I52G[0],ARM[0]
    #plot('AES',0.35,N)
    #N = R73G[1],R74G[1],I52G[1],ARM[1]
    #plot('Blowfish',0.35,N)
    
    N = R73G[2],R74G[2],I52G[2],ARM[2]
    plot('SEED',0.35,N)
