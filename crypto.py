import os
import time
from cryptography.hazmat.primitives.ciphers import Cipher

def key_gen(algorithm,key_len, iv_len, mode):
    print('Algorithm: '+algorithm.name+'\n'+
            'Mode: '+mode.name+'\n'+
            'Key Length: '+str(key_len)+'Bytes -> '+str(key_len*8)+'bits'+'\n'+
            'iv length: '+str(iv_len)+'Bytes -> '+str(iv_len*8)+'bits'+'\n')
    print(('Generating Key').center(80,'-'))

    key = os.urandom(key_len)
    iv = os.urandom(iv_len)
    return Cipher(algorithm(key), mode(iv))

def enc(fname, algorithm):
    print('Encrypting')
    timef = open(algorithm+'.data','a')

    Stime = time.time()
    stime = time.time()
    f = open(fname, 'rb')
    data = f.read()
    f.close()
    etime = time.time()
    timef.write('\n'+str(etime-stime)+' ')
    
    stime = time.time()
    encryptor = cipher.encryptor()
    ct = encryptor.update(data) + encryptor.finalize()
    etime = time.time()
    timef.write(str(etime-stime)+' ')

    stime = time.time()
    f = open(algorithm+'_encrypted','wb')
    f.write(ct)
    f.close()
    etime = time.time()
    timef.write(str(etime-stime)+' ')
    print(str(etime - Stime)+'s')
    timef.close()

def dec(algorithm):
    print('Decrypting')
    timef = open(algorithm+'.data','a')

    Stime = time.time()
    stime = time.time()
    f = open(algorithm+'_encrypted', 'rb')
    data = f.read()
    f.close()
    etime = time.time()
    timef.write(str(etime-stime)+' ')

    stime = time.time()
    decryptor = cipher.decryptor()
    ot = decryptor.update(data) + decryptor.finalize()
    etime = time.time()
    timef.write(str(etime-stime)+' ')

    stime = time.time()
    f = open(algorithm+'_decrypted', 'wb')
    f.write(ot)
    f.close()
    etime = time.time()
    timef.write(str(etime-stime)+' ')
    print(str(etime - Stime)+'s')
    timef.close()

