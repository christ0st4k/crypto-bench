import os
import platform
import time

def info():
    print(('Starting bench time: '+time.ctime(time.time())).center(80,'-'))
    print()
    print(('Bench info:').center(80,' '))
    print(''.center(80,'-'))
    print('CPU Architecture: ' + platform.machine())
    cmd = 'cat /proc/cpuinfo | grep \'model name\' | uniq | cut -d \':\' -f 2-'
    cmd_out = os.popen(cmd)
    print('CPU model name: ')
    for i in cmd_out.readlines():
        print(i)
    print('Platform info: ' + platform.platform())
    print(''.center(80,'-'))

