import os

def sha(filename):
        os.system('shasum -a 512 '+ filename+' > SHA_'+filename+' ')
        f = open('SHA_'+filename,'rb')
        sha = f.read()
        f.close()

        sha = sha.decode("ascii").split()
        sha = sha[0]
        return sha

def sha_match(sha1, sha2):
    if sha1 == sha2:
        print('SHA hashes match'.center(80,'-'))
    else:
        print('Decrypted is not the same with original'.center(80,'-'))

