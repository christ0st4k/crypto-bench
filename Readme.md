How to use this repo:

Required modules apart from default ones.

| Filename     | Modules           |
| ------------ | ----------------- |
| bench.py     | cryptography      |
| crypto.py    | cryptography      |
| validator.py | none              |
| system.py    | none              |
| graph.py     | matplotlib, numpy |

```bash
pip install cryptography matplotlib numpy
```

Edit bench.py :

- in line 10 set the **directory** you want.

- in line 11 the **filename** you want to encrypt

Graphs:
<img src="img/AES.png" alt="AES" style="zoom:50%;" />
<img src="img/Blowfish.png" alt="Blowfish" style="zoom:50%;" />
<img src="img/SEED.png" alt="SEED" style="zoom:50%;" />

| Initials | Full Name           |
| -------- | ------------------- |
| R73G     | AMD Ryzen 3700X     |
| R74G     | AMD Ryzen 4700U     |
| I52G     | Intel Core i5-2310  |
| ARM      | BCM2711 Cortex A-72 |

| Color  | Meaning                      |
| ------ | ---------------------------- |
| Gray   | Read from storage to memory  |
| Red    | Encrypt                      |
| Blue   | Decrypt                      |
| Orange | Write to storage from memory |

